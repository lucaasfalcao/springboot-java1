package br.ufal.lccv.treinamento.exerciciospringboot.services;

import br.ufal.lccv.treinamento.exerciciospringboot.models.dtos.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class StudentService {
    
    private List<Student> students = new LinkedList<>();

    public Student create(Student student) {
        log.info("Estudante que veio: {}", student);
        boolean hasStudent = students.stream().anyMatch((s) -> {
            return s.getCpf().equals(student.getCpf());
        });
        if (hasStudent) {
            throw new RuntimeException("Estudante já existe!");
        }
        students.add(student);
        return student;
    }

    public List<Student> findAll() {
        return students;
    }

    public Student findOne(UUID id) {
        for (Student student:students){
            if (student.getId().equals(id)){
                return student;
            }
        }
        return null;
    }
}
