package br.ufal.lccv.treinamento.exerciciospringboot.controllers;

import br.ufal.lccv.treinamento.exerciciospringboot.models.dtos.Student;
import br.ufal.lccv.treinamento.exerciciospringboot.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StudentController {
    
    private final StudentService service;

    @PostMapping("/")
    public Student create(@Valid @RequestBody Student student) {
        return service.create(student);
    }

    @GetMapping
    public List<Student> findAll() {
        return service.findAll();
    }

    @GetMapping("/ID/{id}")
    public Student findOne(@PathVariable UUID id) {
        return service.findOne(id);
    }
}
