package br.ufal.lccv.treinamento.exerciciospringboot.controllers;

import br.ufal.lccv.treinamento.exerciciospringboot.models.dtos.Teacher;
import br.ufal.lccv.treinamento.exerciciospringboot.services.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/teachers")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TeacherController {
    private final TeacherService service;

    @PostMapping("/")
    public Teacher create(@Valid @RequestBody Teacher teacher) {
        return service.create(teacher);
    }

    @GetMapping
    public List<Teacher> findAll() {
        return service.findAll();
    }

    @GetMapping("/ID/{id}")
    public Teacher findOne(@PathVariable UUID id) {
        return service.findOne(id);
    }
}
