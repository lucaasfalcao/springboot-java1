package br.ufal.lccv.treinamento.exerciciospringboot.controllers;

import br.ufal.lccv.treinamento.exerciciospringboot.models.dtos.Class;
import br.ufal.lccv.treinamento.exerciciospringboot.services.ClassService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/classes")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClassController {
    private final ClassService service;

    @PostMapping("/")
    public Class create(@Valid @RequestBody Class classe) {
        return service.create(classe);
    }

    @GetMapping
    public List<Class> findAll() {
        return service.findAll();
    }

    @GetMapping("/ID/{id}")
    public Class findOne(@PathVariable UUID id) {
        return service.findOne(id);
    }
}
