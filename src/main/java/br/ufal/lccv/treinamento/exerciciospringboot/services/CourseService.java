package br.ufal.lccv.treinamento.exerciciospringboot.services;

import br.ufal.lccv.treinamento.exerciciospringboot.models.dtos.Course;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class CourseService {
    
    private List<Course> courses = new LinkedList<>();

    public Course create(Course course) {
        log.info("Curso que veio: {}", course);
        boolean hasCourse = courses.stream().anyMatch((c) -> {
            return c.getName().equals(course.getName());
        });
        if (hasCourse) {
            throw new RuntimeException("Curso já existe!");
        }
        courses.add(course);
        return course;
    }

    public List<Course> findAll() {
        return courses;
    }

    public Course findOne(UUID id) {
        for (Course course:courses){
            if (course.getId().equals(id)){
                return course;
            }
        }
        return null;
    }
}
