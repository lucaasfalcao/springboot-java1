package br.ufal.lccv.treinamento.exerciciospringboot.services;

import br.ufal.lccv.treinamento.exerciciospringboot.models.dtos.Class;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class ClassService {

    private List<Class> classes = new LinkedList<>();

    public Class create(Class classe) {
        log.info("Disciplina que veio: {}", classe);
        boolean hasClass = classes.stream().anyMatch((c) -> {
            return c.getName().equals(classe.getName());
        });
        if (hasClass) {
            throw new RuntimeException("Disciplina já existe!");
        }
        classes.add(classe);
        return classe;
    }

    public List<Class> findAll() {
        return classes;
    }

    public Class findOne(UUID id) {
        for (Class classe:classes){
            if (classe.getId().equals(id)){
                return classe;
            }
        }
        return null;
    }
}
