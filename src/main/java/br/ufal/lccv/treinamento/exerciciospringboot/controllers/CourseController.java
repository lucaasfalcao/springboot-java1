package br.ufal.lccv.treinamento.exerciciospringboot.controllers;

import br.ufal.lccv.treinamento.exerciciospringboot.models.dtos.Course;
import br.ufal.lccv.treinamento.exerciciospringboot.services.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CourseController {
    private final CourseService service;

    @PostMapping("/")
    public Course create(@Valid @RequestBody Course course) {
        return service.create(course);
    }

    @GetMapping
    public List<Course> findAll() {
        return service.findAll();
    }

    @GetMapping("/ID/{id}")
    public Course findOne(@PathVariable UUID id) {
        return service.findOne(id);
    }
}
