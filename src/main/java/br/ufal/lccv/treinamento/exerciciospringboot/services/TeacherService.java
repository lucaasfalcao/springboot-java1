package br.ufal.lccv.treinamento.exerciciospringboot.services;

import br.ufal.lccv.treinamento.exerciciospringboot.models.dtos.Teacher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class TeacherService {
    
    private List<Teacher> teachers = new LinkedList<>();

    public Teacher create(Teacher teacher) {
        log.info("Professor que veio: {}", teacher);
        boolean hasTeacher = teachers.stream().anyMatch((t) -> {
            return t.getCpf().equals(teacher.getCpf());
        });
        if (hasTeacher) {
            throw new RuntimeException("Professor já existe!");
        }
        teachers.add(teacher);
        return teacher;
    }

    public List<Teacher> findAll() {
        return teachers;
    }

    public Teacher findOne(UUID id) {
        for (Teacher teacher:teachers){
            if (teacher.getId().equals(id)){
                return teacher;
            }
        }
        return null;
    }
}
