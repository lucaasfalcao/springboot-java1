package br.ufal.lccv.treinamento.exerciciospringboot.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    
    private UUID id;
    
    private String cpf;

    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Min(value = 18, message = "Precisa ser maior de idade.")
    private Integer age;

}
