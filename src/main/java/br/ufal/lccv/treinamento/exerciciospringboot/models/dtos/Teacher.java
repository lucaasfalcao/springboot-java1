package br.ufal.lccv.treinamento.exerciciospringboot.models.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {
    
    private UUID id;

    private String cpf;

    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;
    
    private String titration;

}
