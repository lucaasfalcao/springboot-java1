package br.ufal.lccv.treinamento.exerciciospringboot.config;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.LinkedList;
import java.util.List;

@RestControllerAdvice
public class ExceptionHandlerConfig {
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(RuntimeException.class)
    public String runtimeHandler(RuntimeException ex) {
        return "Deu um erro de runtime!!!!! => " + ex.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<String> handleValidation(MethodArgumentNotValidException ex) {
        final List<String> errors = new LinkedList<>();
        ex.getFieldErrors().forEach(error -> {
            errors.add(error.getDefaultMessage());
        });
        return errors;
    }
}
