# Especificações dos endpoints

## Endpoint POST

Para criar um novo aluno, curso, professor, ou disciplina, basta ir no arquivo test.http, inserir os dados referentes no JSON exemplificado e enviar a requisição. Como resposta retornará o objeto criado com os respectivos dados inseridos.

## Endpoint GET (para todos)

Para visualizar todos os alunos, cursos, professores, ou disciplinas existentes, basta ir no arquivo test.http e enviar a requisição desejada. Como resposta retornará uma lista com todos os respectivos objetos e seus dados existentes.

## Endpoint GET (para um)

Para visualizar um aluno, curso, professor, ou disciplina existente, basta ir no arquivo test.http e enviar a requisição inserindo o ID da entidade desejada ao final do path da requisição, como está exemplificado. Como resposta retornará o respectivo objeto com os seus dados.